import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../main.dart';

class LoginsScreen extends StatelessWidget {
  const LoginsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: [
          Positioned(
            top: media(context).height * 0.0,
            left: 40,
            child: Lottie.asset(
              'assets/87422-color-wheel.json',
            ),
          ),
//if u want to blur the background un comment this part
          // Positioned.fill(
          //     child: BackdropFilter(
          //         child: SizedBox(),
          //         filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5))),
          Positioned(
              width: media(context).width * 0.8,
              top: media(context).height * 0.1,
              left: 40,
              child: const Column(
                children: [
                  Text(
                    overflow: TextOverflow.clip,
                    'Dive in & \nDiscover the world of art',
                    style: TextStyle(
                        fontFamily: 'Poppins',
                        fontSize: 50,
                        fontWeight: FontWeight.bold),
                  ),
                  Divider(thickness: 2),
                  Text(
                    overflow: TextOverflow.clip,
                    'arts,Historical paint photographs and more than thin in just one place\n artiango is the life discovring youpa',
                    style: TextStyle(
                        fontFamily: 'Poppins',
                        fontSize: 18,
                        fontWeight: FontWeight.normal),
                  ),
                ],
              )),
          Positioned(
              width: media(context).width * 0.8,
              top: media(context).height * 0.6,
              left: 40,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    elevation: 10,
                    fixedSize: Size(media(context).width * 0.7,
                        media(context).width * 0.15),
                    disabledBackgroundColor: Colors.transparent,
                    backgroundColor: Colors.deepPurple.withOpacity(0.8)),
                onPressed: () {
                  showGeneralDialog(
                      barrierDismissible: true,
                      barrierLabel: "hi",
                      context: context,
                      pageBuilder: (context, animation, animation1) => Center(
                          child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.white.withOpacity(0.95),
                                  borderRadius: BorderRadius.circular(20)),
                              width: media(context).width * 0.85,
                              height: media(context).height * 0.6,
                              child: Scaffold(
                                  backgroundColor: Colors.transparent,
                                  body: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        const Text(
                                          overflow: TextOverflow.clip,
                                          'Log In',
                                          style: TextStyle(
                                              fontFamily: 'Poppins',
                                              fontSize: 35,
                                              color: Colors.pink,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 20.0),
                                          child: TextFormField(
                                            decoration: const InputDecoration(
                                                label: Text('Email'),
                                                border: OutlineInputBorder()),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 20.0),
                                          child: TextFormField(
                                            decoration: const InputDecoration(
                                                label: Text('Password'),
                                                border: OutlineInputBorder()),
                                          ),
                                        ),
                                        const Row(
                                          children: [
                                            Expanded(
                                                child: Divider(thickness: 2)),
                                            Text(
                                              overflow: TextOverflow.clip,
                                              'or',
                                              style: TextStyle(
                                                  fontFamily: 'Poppins',
                                                  fontSize: 20,
                                                  color: Colors.grey,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Expanded(
                                                child: Divider(
                                              thickness: 2,
                                            )),
                                          ],
                                        ),
                                        TextButton(
                                          onPressed: () {},
                                          child: const Text(
                                            overflow: TextOverflow.clip,
                                            'Register for Free',
                                            style: TextStyle(
                                                fontFamily: 'Poppins',
                                                fontSize: 20,
                                                color: Colors.deepPurple,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceAround,
                                          children: [
                                            InkWell(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              onTap: () {},
                                              child: Image.asset(
                                                  "assets/Google_icon-icons.com_66793.png",
                                                  width: 35),
                                            ),
                                            InkWell(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              onTap: () {},
                                              child: Image.asset(
                                                  "assets/figma_logo_icon_147289.png",
                                                  width: 35),
                                            ),
                                            InkWell(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              onTap: () {},
                                              child: Image.asset(
                                                  "assets/git_plain_wordmark_logo_icon_146508.png",
                                                  width: 35),
                                            ),
                                            InkWell(
                                              borderRadius:
                                                  BorderRadius.circular(30),
                                              onTap: () {},
                                              child: Image.asset(
                                                  "assets/circle-dribble_icon-icons.com_66836.png",
                                                  width: 35),
                                            ),
                                          ],
                                        ),
                                        const SizedBox(
                                          height: 30,
                                        )
                                      ],
                                    ),
                                  )))));
                },
                child: const Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      overflow: TextOverflow.clip,
                      'Get Started',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Icon(CupertinoIcons.arrow_right_circle)
                  ],
                ),
              )),
        ],
      ),
    );
  }
}
