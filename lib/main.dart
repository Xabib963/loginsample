import 'package:flutter/material.dart';
import 'package:riveapp/PaintAppLogin/logins.dart';

void main() {
  runApp(const MyApp());
}

Size media(BuildContext context) {
  return MediaQuery.of(context).size;
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.pinkAccent),
      ),
      home: const LoginsScreen(),
    );
  }
}
